import {h, Component, render, createRef} from 'preact'
import htm from 'htm'
import iassign from 'immutable-assign'
import org from './org-parser'
import {drag, allowDrop} from './dragDrop'
import ruby from '../data/ruby.json'
const html = htm.bind(h)

function Table ({header, hiddenColumns, addColumn, children}) {
  return html`
    <div class="table-wrapper">
      <table>${header}<tbody>${children}</tbody></table>
      <${AddColumnWidget} hiddenColumns=${hiddenColumns} addColumn=${addColumn} />
    </div>`
}

class AddColumnWidget extends Component {
  constructor() {
    super()
    this.state = {expanded: false}
    this.ref = createRef()
  }
  expand() {
    this.setState({expanded: true})
  }
  cancel() {
    this.setState({expanded: false})
  }
  blur({relatedTarget}) {
    if(!this.ref.current.contains(relatedTarget)) {
      this.cancel()
    }
  }
  render({hiddenColumns, addColumn}) {
    const {expanded} = this.state
    const wrap = fn => name => {
      fn(name)
      this.cancel()
    }
    return html`
      <span class="add-column" ref=${this.ref}>
        <button title="Add a column"
                class="add"
                onClick=${this.expand.bind(this)}
                onBlur=${this.blur.bind(this)}
                disabled=${hiddenColumns.length == 0} />
        <ul class="menu ${expanded ? 'expanded' : ''}">
          ${hiddenColumns.map(column => AddColumnListItem({column, addColumn: wrap(addColumn(column))}))}
          <button title="Cancel" onClick=${() => this.cancel()} class="cancel" />
        </ul>
      </span>`
  }
}

function AddColumnListItem({column, addColumn}) {
  return html`
    <li title="Add column “${column}”"
        onClick=${addColumn}>
      <input type="text" value="+ ${column}" />
    </li>`
}

function Row ({row, columns}) {
  return html`<tr>${columns.map(column => html`<td><${OrgStyledText} children=${row[column]} /></td>`)}</tr>`
}

function TableHead({columns, drop, del}) {
  return html`<thead>${columns.map(column => TableHeader(column, drop, del))}</thead>`
}

function TableHeader (column, drop, del) {
  const ref = createRef()
  return html`
    <th draggable=${true}
        onDragStart=${drag}
        onDragOver=${allowDrop}
        onDrop=${drop}
        ref=${ref}
        data-name=${column}
        key=${column}>
      ${column}
      <span class="handle" title="Move column" />
      <span class="delete" title="Hide column: “${column}”" onClick=${del(column)} />
    </th>`
}

function OrgStyledExpr (expr) {
  switch (expr.type) {
  case 'Code':
    return html`<code class="org expr">${expr.text}</code>`
  case 'String':
    return html`<span class="org expr">${expr.text}</span>`
  }
  return html``
}

function OrgStyledText ({children}) {
  if(children[0] === "") {
    return html``
  }
  let line = org.parse(children, {startRule: 'startLine'})
  return html`<span class="org line">${line.map(OrgStyledExpr)}</span>`
}

class App extends Component {
  constructor(...args) {
    super(...args)
    this.allHeadings = ruby.dataStructures.headings;
    this.state = ruby
  }

  deleteColumn(name) {
    const index = this.state.headings.indexOf(name)
    this.setState(iassign(this.state, s => s.dataStructures.headings, headings => {
      headings.splice(index, 1)
      return headings
    }))
  }

  addColumn(name) {
    this.setState(iassign(this.state, s => s.dataStructures.headings, headings => {
      headings.push(name)
      return headings
    }))
  }

  moveColumn(name, before) {
    const {dataStructures: {headings}} = this.state
    const nameIndex = headings.indexOf(name)
    const beforeIndex = headings.indexOf(before)
    this.setState(iassign(this.state, s => s.dataStructures.headings, headings => {
      headings.splice(nameIndex, 1)
      headings.splice(beforeIndex, 0, name)
      return headings
    }))
  }

  get hiddenColumns() {
    return this.allHeadings.filter(head => this.state.dataStructures.headings.indexOf(head) < 0)
  }

  render() {
    const {dataStructures: {headings: columns, rows}, title} = this.state
    const deleteColumn = name => () => this.deleteColumn(name)
    const addColumn = name => () => this.addColumn(name)
    const drop = event => {
      const {target, dataTransfer} = event
      event.preventDefault()
      this.moveColumn(dataTransfer.getData('text'), target.dataset.name)
    }
    return html`<div class="app-wrapper">
                  <h1>${title}</h1>
                  <${Table}
                    header=${TableHead({columns, drop, del: deleteColumn})}
                    hiddenColumns=${this.hiddenColumns}
                    addColumn=${addColumn}
                    children=${rows.map(row => Row({row, columns}))} />
                </div>`
  }
}

render(html`<${App} />`, document.getElementById('app'))
