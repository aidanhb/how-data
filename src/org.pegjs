start
  = Paragraph
  / EOF { return {type: "Paragraph", children: []} }

Paragraph
  = children:Line* {
    return {type: "Paragraph", children}
  }

startLine
  = Line
  / EOF { return [] }

Line
  = children:Expr+ LineTerminator {
    return children
  }

LineTerminator "(eol)"
  = "\n"
  / "\r\n"
  / "\r"
  / EOF

EOF
  = !.

Expr
  = CodeText
  / String

CodeText
  = "~" text:[^~]+ "~" {
    return {type: "Code", text: text.join('')}
  }

String
  = text:Char+ {
    return {type: "String", text: text.join('')}
  }

Char
  = [^~\n]
