#+TITLE: Ruby Data Structures

This is a table of common data structures in Ruby, to help you learn & memorize their APIs.

#+NAME: tab:data-structures
| data structure | instantiate     | literal         | count                          | get                                  | add                        | visit                                   | test membership | remove                              |
|----------------+-----------------+-----------------+--------------------------------+--------------------------------------+----------------------------+-----------------------------------------+-----------------+-------------------------------------|
| (enumerable)   | n/a (abstract)  | n/a             | ~#count~                       | ~#find~, ~#take~                     | n/a                        | ~#each~, ~#find_all~, ~#map~, ~#reduce~ | ~#member?~      | ~#drop~, ~#reject~, ~#uniq~         |
| Hash           | ~Hash.new~      | ~{ key: val }~  | enumerable, ~#length~, ~#size~ | ~#fetch~, ~#dig~, ~#slice~, ~#[key]~ | ~#store~, ~[key]=val~      | enumerable, ~#each_key~                 | enumerable      | ~#shift~, ~#delete~, ~#delete_if~   |
| Array          | ~Array.new~     | ~[, ]~          | enumerable, ~#length~, ~#size~ | ~#fetch~, ~#dig~, ~#slice~           | ~#push~, ~#unshift~, ~#<<~ | enumerable, ~#each_index~               | enumerable      | ~#pop~, ~#shift~, ~#delete_{at,if}~ |
| String         | ~String.new~    | ~"content"~     | ~#length~, ~#size~             | ~#bytes~, ~#getbyte~, ~#slice~       | ~#concat~, ~#<<~           | ~#each_{byte,char}~, ~#split~           | ~#include?~     | ~#chop~, ~#strip~                   |
| Set*           | ~Set.new~       | ~Set[, ]~       | enumerable, ~#length~, ~#size~ | enumerable                           | ~#add~, ~#union~, ~#merge~ | enumerable                              | ~&~, ~<~        | ~#delete~, ~#difference~            |
| SortedSet*     | ~SortedSet.new~ | ~SortedSet[, ]~ | enumerable                     | enumerable                           | Set                        | enumerable                              | Set             | Set                                 |
| (queue)        | use vector      | ~[, ]~          | enumerable                     | Array                                | Array                      | enumerable                              | enumerable      | Array                               |

 * use ~require 'set'~

#+NAME: tab:links
| data structure | API method | link                                                           | title                                                    |
|----------------+------------+----------------------------------------------------------------+----------------------------------------------------------|
| (enumerable)   | ~#count~   | https://ruby-doc.org/core-2.6.5/Enumerable.html#method-i-count | Returns the number of items in enum through enumeration. |
