#!/usr/bin/env node

// patterns
/**
 * A title is the literal string "#+TITLE: " followed by the title of the document.
 */
const title_regex = /#\+TITLE: (.+)/

/**
 * A name is the literal string "#+NAME: tab" followed by the name of a table.
 */
const name_regex = /#\+NAME: tab:(.+)/

/**
 * A row begins with the character "|" but we exlude rows which begin "|-"
 * because they are probably line separators.
 */
const row_regex = /^\|[^-].+$/

/**
 * Transform an org-mode pipe table row into an array.
 * @example
 * // returns ["first", "sec\\|ond", "third"]
 * cleanRow("| first   | sec\\|ond | third     |")
 * @param {string} row - A single line of the table, representing a row
 * @returns {array}
 */
function cleanRow(row) {
  return row.split(/(?<!\\)\|/).filter(Boolean).map(field => field.trim())
}

/**
 * Returns a function which attempts to match the given regex against a line of
 * text, returning the first group in that match.
 * @example
 * // returns "3"
 * matchPart(/([0-9]+)/)('cats be like :3')
 */
function matchPart(regex) {
  return line => {
    let result = typeof(line) === "string" && line.match(regex)
    if (result && result[1]) {
      return result[1]
    } else {
      return null
    }
  }
}

/**
 * Attempts to match an org-mode title, returning the title text if it succeeds.
 * @example
 * // returns "Ruby"
 * title("#+TITLE: Ruby")
 * @example
 * // returns null
 * title("| language |")
 * @param {string} line - A single line of text to process.
 * @returns {string}
 */
const title = matchPart(title_regex)

/**
 * Attempts to match an org-mode table name, returning name text if it succeeds.
 * @example
 * // returns "Data Structures"
 * name("#+NAME: tab:Data Structures")
 * @example
 * // returns null
 * title("| language |")
 * @param {string} line - A single line of text to process.
 * @returns {string}
 */
const name = matchPart(name_regex)

/**
 * Attempts to match an org-mode table header, returning it as an array if it
 * succeeds.
 * @example
 * // returns ["name", "length"]
 * headings("| name | length |")
 * @param {string} line - A single line of text to process.
 * @returns {array}
 */
function headings(line) {
  let result = typeof(line) === "string" && line.match(row_regex)
  if (result) {
    return cleanRow(line)
  } else {
    return null
  }
}

/**
 * Attempts to match a row of an org-mode table, returning it an object if it
 * suceeds.
 * @example
 * // returns {name: "vector", length: "#count"}
 * row("| vector | #count |", ["name", "length"])
 * @param {string} line - A single line of text to process.
 * @param {array} headings - The table headings for each column.
 * @returns {object}
 */
function row(line, headings) {
  let result = typeof(line) === "string" && line.match(row_regex)
  if(result) {
    return cleanRow(line).reduce((acc, cur, i) => {
      acc[headings[i]] = cur
      return acc
    }, {})
  } else {
    return null
  }
}

/**
 * Creates a camel case string, common in the nodejs ecosystem, from a string
 * with dash delimiters, common in org-mode and many places.
 * @example
 * // returns "tableToJson"
 * dashToCamelCase('table-to-json')
 * @param {string} string - The string to convert to camel case.
 * @returns {string}
 */
function dashToCamelCase(string) {
  return string.replace(/-(.)/g, (_, firstChar) => firstChar.toUpperCase())
}

/**
 * Enum for machine states while reading a table file.
 * @readonly
 * @enum {string}
 */
const STATES = {
  READ_TITLE: "TITLE",
  READ_NAME: "NAME",
  READ_HEADINGS: "HEADINGS",
  READ_ROWS: "ROWS"
}

/**
 * Turns an org document with pipe table into an object.
 * @param {string} string - An org-mode string, containing a title & a single pipe table.
 * @returns {object}
 */
function loadTable(string, useCamelCase) {
  let state = STATES.READ_TITLE
  let result = {}
  let current = {rows: []}
  const lines = string.split('\n')
  lines.forEach(line => {
    switch(state) {
    case STATES.READ_TITLE:
      let t = title(line)
      if(t) {
        result.title = t
        state = STATES.READ_NAME
      }
      break
    case STATES.READ_NAME:
      let n = name(line)
      if(n) {
        current.name = useCamelCase ? dashToCamelCase(n) : n
        state = STATES.READ_HEADINGS
      }
    case STATES.READ_HEADINGS:
      let h = headings(line)
      if(h) {
        current.headings = h
        state = STATES.READ_ROWS
      }
      break
    case STATES.READ_ROWS:
      let r = row(line, current.headings)
      if(r) {
        current.rows.push(r)
      } else if(line.length == 0) {
        // empty line: look for another table
        result[current.name] = current
        current = {rows: []}
        state = STATES.READ_NAME
      }
    }
  })
  return result
}

function parseOptions() {
  const cli = require('snack-cli')
  return cli
    .name('table-to-json')
    .version('1.0.0')
    .usage('<options>')
    .description('Extract data from a document with an org-mode table to JSON')
    .option('-f, --file <file>', 'optional file to read from', '')
    .option('-o, --output <file>', 'optional file to write to', '')
    .option('-p, --pretty-print', 'print more human-readable JSON')
    .option('-c, --camel-case', 'convert table names to camel case')
    .parse()
}

function main() {
  const process = require('process')
  const path = require('path')
  const fs = require('fs')
  const options = parseOptions()
  // if no file provided, use stdin (0)
  const file = options.file ? path.join(process.cwd(), options.file) : 0
  const table = loadTable(fs.readFileSync(file).toString(), options.camelCase)
  const json = options.prettyPrint ? JSON.stringify(table, null, 2) : JSON.stringify(table)
  if (options.output) {
    fs.writeFileSync(path.join(process.cwd(), options.output), json + '\n')
  } else {
    process.stdout.write(json + '\n')
  }
}

main()
